<?php

    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');
    
    $sheep = new Animal("shaun");

    echo "animal's name : " . $sheep->name . "<br>"; 
    echo "legs : " . $sheep->legs . "<br>"; 
    echo "cold blooded : " . $sheep->cold_blooded . "<br><br><br>"; 

    $sungokong = new Ape ("kera sakti");
    echo "animal's name : " . $sungokong->name . "<br>"; 
    echo "legs : " . $sungokong->legs . "<br>"; 
    echo "cold blooded : " . $sungokong->cold_blooded . "<br>"; 
    echo "Yell : " . $sungokong->yell . "<br><br><br>"; 

    $kodok = new Frog("buduk");
    echo "animal's name : " . $kodok->name . "<br>"; 
    echo "legs : " . $kodok->legs . "<br>"; 
    echo "cold blooded : " . $kodok->cold_blooded . "<br>"; 
    echo "jump : " . $kodok->jump . "<br><br><br>"; 
  
  
?>